const express = require('express')
const app = express()




//Explicar esta nueva forma de requiere que asigna directamente la función a la variable
const getRandomInt = require('./librerias/numeros')

const env = require("./variables.entorno.json")
const node_env = process.env.NODE_ENV || 'dev'
const entorno = env[node_env];



//app.use(express.static('public'));
app.use(express.static(__dirname + '/public'));
app.use(express.json()) // To parse the incoming requests with JSON payloads
app.use(express.urlencoded({extended: true}));



 
// app.get('/', function (req, res) {
//   res.send('Hello World')
// });


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/public'+ "/index.html");
})

app.post("/", (req, res) => {
    debugger;
    console.log(req.body);
    res.send("Thank you for subscribing");
  });

app.get('/users', function (req, res) {
    res.send('Listado de usuarios')
})

app.post('/users', function (req, res) {
    res.send('Endpoint para agregar usuarios')    
})

app.delete('/users', function (req, res) {
    res.send('Endpoint para eliminar usuarios')    
})

app.put('/users', function (req, res) {
    res.send('Endpoint para actualizar usuarios')    
})

app.get('/json', function (req, res) {
    const mi_respuesta = {msj: 'Respuesta correcta'}
    res.json(mi_respuesta)    
})

app.get('/pares', function (req, res) {
    const numero = getRandomInt(1, 100)
    let mi_respuesta = {msj: numero}
    if(numero % 2 == 0) {
        mi_respuesta.par = true;
        res.status(200)
    }else{
        res.status(400)
        mi_respuesta.par = false;
    }
    res.json(mi_respuesta)    
})

 
app.listen(entorno.port, function() {
    console.log(`Express corriendo en http://localhost:${entorno.port}/index.html`)
})