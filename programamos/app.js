const express = require('express')
const app = express()

let telefonos = [{
    marca: "Samsung",
    modelo: "S10",
    gama: "Alta",
    pantalla: "19:9",
    sistema_operativo: "Android",
    precio: "1000"
},{
    marca: "Iphone",
    modelo: "12 Pro",
    gama: "Alta",
    pantalla: "OLED",
    sistema_operativo: "iOS",
    precio: "1200"
},{
    marca: "Nokia",
    modelo: "1100",
    gama: "Baja",
    pantalla: "LCD",
    sistema_operativo: "Series 40",
    precio: "150"
},{
    marca: "Xiaomi",
    modelo: "Mi 9T Pro",
    gama: "Media",
    pantalla: "LCD",
    sistema_operativo: "Android 9 Pie",
    precio: "450"
}]

app.get('/', function (req, res) {
  res.json(telefonos)
})

app.get('/medio', function (req, res) {
    res.json(telefonos.splice(0,(telefonos.length/2)))
})
 
app.listen(3000, function() {
    console.log(`Express corriendo en http://localhost:3000`)
})